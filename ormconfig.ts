import { DataSource } from "typeorm";

export const connectionSource = new DataSource({
    "type": "postgres",
    "host": "localhost",
    "port": 5432,
    "username": "omarc",
    "password": "P3r1r3r14",
    "database": "restaurante_periferia",
    "entities": ["src/**/*.entity.ts"],
    "synchronize": false,
    "migrations": ["src/database/*.ts"],
    "migrationsTableName": "migration_db_periferia"
});