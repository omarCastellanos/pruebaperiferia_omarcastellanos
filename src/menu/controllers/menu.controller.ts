import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { MenuService } from '../services/menu.service';

@Controller('api/menu')
export class MenuController {

    constructor(
        private menuService: MenuService
    ){}

    @Post('/create')
    Create(@Body() body: any) {
        return this.menuService.create(body);
        //return [1, 2, 3];
    }

    @Get(':id')
    getOne(@Param('id') id: number){
        return this.menuService.searchOne(id);
    }

    @Get()
    getAll(){
        return this.menuService.searchAll();
    }

    @Put(':id')
    update(@Param('id') id: number, @Body() body: any) {
        return this.menuService.update(id, body);
    }
}
