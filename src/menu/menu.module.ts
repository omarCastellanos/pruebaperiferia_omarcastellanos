import { Module } from '@nestjs/common';
import { MenuController } from './controllers/menu.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MenuService } from './services/menu.service';
import { Menu } from './entities/menu.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Menu])
  ],
  providers: [MenuService],
  controllers: [MenuController]
})
export class MenuModule {}
