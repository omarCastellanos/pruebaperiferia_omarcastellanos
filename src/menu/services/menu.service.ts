import { Injectable } from '@nestjs/common';
import { Menu } from '../entities/menu.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MenuService {
    constructor(
        @InjectRepository(Menu) private MenuRepo: Repository<Menu>
    ) {}

    searchAll() {
        return this.MenuRepo.find();
    }

    searchOne(id: number) {
        return this.MenuRepo.findOneBy({id: id});
    }

    create(body: any) {
        const newMenu = this.MenuRepo.create({
            description: body.description,
            restaurant : body.restaurant,
            status: body.status    
        })
        return this.MenuRepo.save(newMenu)
    }

    async update(id: number, body: any){
        const order = await this.MenuRepo.findOneBy({id: id});
        this.MenuRepo.merge(order, body);
        return this.MenuRepo.save(order);
    };

}
