import { OrderDelivery } from 'src/orders/entities/order.entity';
import { Restaurant } from 'src/restaurant/entities/restaurant.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';


@Entity()
export class Menu {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    description: string;

    @Column()
    status: boolean;

    @ManyToOne(() => Restaurant, (restaurant) => restaurant.menus)
    restaurant: Restaurant;

    @OneToMany(() => OrderDelivery, (orderDelivery) => orderDelivery.menu)
    orderDeliverys: OrderDelivery[]
}