import { Controller, Param, Post, Body, Get, Put } from '@nestjs/common';
import {RestaurantService } from '../services/restaurant.service';

@Controller('api/restaurant')
export class RestaurantController {
    
    constructor(
        private retaurantService: RestaurantService
    ){}

    @Post('/create')
    Create(@Body() body: any) {
        return this.retaurantService.create(body);
    }

    @Get(':id')
    getOne(@Param('id') id: number){
        return this.retaurantService.searchOne(id);
    }

    
    @Get()
    getAll(){
        return this.retaurantService.searchAll();
    }

    @Put(':id')
    update(@Param('id') id: number, @Body() body: any) {
        return this.retaurantService.update(id, body);
    }

}
