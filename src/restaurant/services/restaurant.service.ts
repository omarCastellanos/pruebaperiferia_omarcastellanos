import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Restaurant } from '../entities/restaurant.entity';


@Injectable()
export class RestaurantService {
    constructor(
        @InjectRepository(Restaurant) private RestaurantRepo: Repository<Restaurant>
    ){}

    searchAll() {
        return this.RestaurantRepo.find();
    }

    searchOne(id: number) {
        return this.RestaurantRepo.findOneBy({id: id});
    }

    create(body: any) {
        const newRestaurant = this.RestaurantRepo.create({
            name: body.name,
            status: body.status
        })
        return this.RestaurantRepo.save(newRestaurant)
    }   

    async update(id: number, body: any){
        const restau = await this.RestaurantRepo.findOneBy({id: id});
        this.RestaurantRepo.merge(restau, body);
        return this.RestaurantRepo.save(restau);
    };
}
