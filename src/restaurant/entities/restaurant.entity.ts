import { Menu } from 'src/menu/entities/menu.entity';
import { OrderDelivery } from 'src/orders/entities/order.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';


@Entity()
export class Restaurant {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    name: string;

    @Column()
    status: boolean; 

    @OneToMany(() => Menu, (menu) => menu.restaurant)
    menus: Menu[]

    @OneToMany(() => OrderDelivery, (orderDelivery) => orderDelivery.restaurant)
    orderDeliverys: OrderDelivery[]
}