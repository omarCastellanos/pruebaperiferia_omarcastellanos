import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrations1700961086577 implements MigrationInterface {
    name = 'Migrations1700961086577'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "delivery" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "status" boolean NOT NULL, CONSTRAINT "PK_ffad7bf84e68716cd9af89003b0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "pay" ("id" SERIAL NOT NULL, "valueOrder" integer NOT NULL, "status" boolean NOT NULL, CONSTRAINT "PK_84be6c3589bea5029defdc144eb" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "restaurant" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "status" boolean NOT NULL, CONSTRAINT "PK_649e250d8b8165cb406d99aa30f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "order_delivery" ("id" SERIAL NOT NULL, "user_name" character varying NOT NULL, "email" character varying NOT NULL, "address" character varying NOT NULL, "restaurantId" integer, "menuId" integer, "deliveryId" integer, "payId" integer, CONSTRAINT "REL_3dff3af5ee0f75670f016def8a" UNIQUE ("payId"), CONSTRAINT "PK_962eec87d3d029c51525f259fba" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "menu" ("id" SERIAL NOT NULL, "description" character varying NOT NULL, "status" boolean NOT NULL, "restaurantId" integer, CONSTRAINT "PK_35b2a8f47d153ff7a41860cceeb" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "order_delivery" ADD CONSTRAINT "FK_9da8fadfc84c202a6dc2158a4ae" FOREIGN KEY ("restaurantId") REFERENCES "restaurant"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "order_delivery" ADD CONSTRAINT "FK_67b109c096ec0d6a45bcbe29f56" FOREIGN KEY ("menuId") REFERENCES "menu"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "order_delivery" ADD CONSTRAINT "FK_bcb9373ac8c0349c5da7fdc8df3" FOREIGN KEY ("deliveryId") REFERENCES "delivery"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "order_delivery" ADD CONSTRAINT "FK_3dff3af5ee0f75670f016def8af" FOREIGN KEY ("payId") REFERENCES "pay"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "menu" ADD CONSTRAINT "FK_085156de3c3a44eba017a6a0846" FOREIGN KEY ("restaurantId") REFERENCES "restaurant"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "menu" DROP CONSTRAINT "FK_085156de3c3a44eba017a6a0846"`);
        await queryRunner.query(`ALTER TABLE "order_delivery" DROP CONSTRAINT "FK_3dff3af5ee0f75670f016def8af"`);
        await queryRunner.query(`ALTER TABLE "order_delivery" DROP CONSTRAINT "FK_bcb9373ac8c0349c5da7fdc8df3"`);
        await queryRunner.query(`ALTER TABLE "order_delivery" DROP CONSTRAINT "FK_67b109c096ec0d6a45bcbe29f56"`);
        await queryRunner.query(`ALTER TABLE "order_delivery" DROP CONSTRAINT "FK_9da8fadfc84c202a6dc2158a4ae"`);
        await queryRunner.query(`DROP TABLE "menu"`);
        await queryRunner.query(`DROP TABLE "order_delivery"`);
        await queryRunner.query(`DROP TABLE "restaurant"`);
        await queryRunner.query(`DROP TABLE "pay"`);
        await queryRunner.query(`DROP TABLE "delivery"`);
    }

}
