import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeliveryService } from '../services/delivery.service';

@Controller('api/delivery')
export class DeliveryController {
    constructor(
        private deliveryService: DeliveryService
    ){}

    @Post('/create')
    Create(@Body() body: any) {
        return this.deliveryService.create(body);
        //return [1, 2, 3];
    }

    @Get(':id')
    getOne(@Param('id') id: number){
        return this.deliveryService.searchOne(id);
    }

    @Get()
    getAll(){
        return this.deliveryService.searchAll();
    }

}
