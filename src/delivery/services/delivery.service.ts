import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Delivery } from '../entities/delivery.entity';
import { Repository } from 'typeorm';

@Injectable()
export class DeliveryService {
    constructor(
        @InjectRepository(Delivery) private DeliveryRepo: Repository<Delivery>
    ){}

    searchAll() {
        return this.DeliveryRepo.find();
    }

    searchOne(id: number) {
        return this.DeliveryRepo.findOneBy({id: id});
    }

    create(body: any) {
        const newDelivery = this.DeliveryRepo.create({
            name: body.name,
            status: body.status
        })
        return this.DeliveryRepo.save(newDelivery)
    }

}
