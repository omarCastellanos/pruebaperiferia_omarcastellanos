import { OrderDelivery } from 'src/orders/entities/order.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';


@Entity()
export class Delivery {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    name: string;

    @Column()
    status: boolean;
    
    @OneToMany(() => OrderDelivery, (orderDelivery) => orderDelivery.delivery)
    orderDeliverys: OrderDelivery[]
}