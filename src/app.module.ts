import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OrdersModule } from './orders/orders.module';
import { RestaurantModule } from './restaurant/restaurant.module';
import { MenuModule } from './menu/menu.module';
import { DeliveryModule } from './delivery/delivery.module';
import { PayModule } from './pay/pay.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'omarc',
      password: 'P3r1r3r14',
      database: 'restaurante_periferia',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: false,
      retryDelay: 5000,
      retryAttempts : 5
    }),    
    OrdersModule, RestaurantModule, MenuModule, DeliveryModule, PayModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
