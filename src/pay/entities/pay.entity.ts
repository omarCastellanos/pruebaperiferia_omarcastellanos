import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';


@Entity()
export class Pay {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    valueOrder: number;

    @Column()
    status: boolean; 
}