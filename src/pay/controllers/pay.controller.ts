import { Controller, Param, Post, Body, Get } from '@nestjs/common';
import { PayService } from '../services/pay.service';

@Controller('api/pay')
export class PayController {

    constructor(
        private payService: PayService
    ){}

    @Post('/create')
    Create(@Body() body: any) {
        return this.payService.create(body);
    }

    @Get(':id')
    getOne(@Param('id') id: number){
        return this.payService.searchOne(id);
    }

    
    @Get()
    getAll(){
        return this.payService.searchAll();
    }

}
