import { Module } from '@nestjs/common';
import { PayService } from './services/pay.service';
import { PayController } from './controllers/pay.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Pay } from './entities/pay.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Pay])
  ],
  providers: [PayService],
  controllers: [PayController]
})
export class PayModule {}
