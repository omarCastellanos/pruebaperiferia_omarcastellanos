import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Pay } from '../entities/pay.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PayService {
    constructor(
        @InjectRepository(Pay) private PayRepo: Repository<Pay>
    ){}

    searchAll() {
        return this.PayRepo.find();
    }

    searchOne(id: number) {
        return this.PayRepo.findOneBy({id: id});
    }

    create(body: any) {
        const newPay= this.PayRepo.create({
            valueOrder: body.valueOrder,
            status: body.status
        })
        return this.PayRepo.save(newPay)
    }  

}
