import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm'
import { OrderDelivery } from '../entities/order.entity';
import { Restaurant } from 'src/restaurant/entities/restaurant.entity';


@Injectable()
export class OrdersService {
    constructor(
        @InjectRepository(OrderDelivery) private OrderRepo: Repository<OrderDelivery>
    ) {}

    searchAll() {
        return this.OrderRepo.find();
    }

    async searchOne(id: number) {
        const entity = await this.OrderRepo
            .createQueryBuilder('OrderDelivery')
            .select(
                'ord.user_name as Usuario, \
                res.name as Restaurante, \
                men.description as Menu, \
                del.name as Repartidor, \
                py.valueOrder as Valor',
            )
            .from('OrderDelivery', 'ord')
            .leftJoin('ord.restaurant', 'res')
            .leftJoin('ord.menu', 'men')
            .leftJoin('ord.delivery', 'del')
            .leftJoin('ord.pay', 'py')
            .where("ord.id = :id", { id : id })
            .getRawOne();

            return entity;
    }

    create(body: any) {
        const newOrder = this.OrderRepo.create({
            user_name: body.user_name,
            email: body.email,
            address: body.address,
            delivery: body.delivery,
            restaurant: body.restaurant,
            menu: body.menu,
            pay: body.pay           
        })
        return this.OrderRepo.save(newOrder)
    }

    async update(id: number, body: any){
        const order = await this.OrderRepo.findOneBy({id: id});
        this.OrderRepo.merge(order, body);
        return this.OrderRepo.save(order);
    };

}
