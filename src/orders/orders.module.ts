import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrdersService } from './services/orders.service';
import { OrdersController } from './controllers/orders.controller';
import { OrderDelivery } from './entities/order.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([OrderDelivery])
  ],
  providers: [OrdersService],
  controllers: [OrdersController]
})
export class OrdersModule {}
