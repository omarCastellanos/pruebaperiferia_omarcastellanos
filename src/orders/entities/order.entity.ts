import { Delivery } from 'src/delivery/entities/delivery.entity';
import { Menu } from 'src/menu/entities/menu.entity';
import { Pay } from 'src/pay/entities/pay.entity';
import { Restaurant } from 'src/restaurant/entities/restaurant.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, ManyToOne } from 'typeorm';


@Entity()
export class OrderDelivery {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    user_name: string;

    @Column()
    email: string;

    @Column()
    address: string;

    @ManyToOne(() => Restaurant, (restaurant) => restaurant.orderDeliverys)
    restaurant: Restaurant;

    @ManyToOne(() => Menu, (menu) => menu.orderDeliverys)
    menu: Menu;

    @ManyToOne(() => Delivery, (delivery) => delivery.orderDeliverys)
    delivery: Delivery;

    @OneToOne(() => Pay)
    @JoinColumn()
    pay: Pay
}