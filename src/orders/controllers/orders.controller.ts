import { Controller, Param, Post, Body, Get, Put } from '@nestjs/common';
import { OrdersService } from '../services/orders.service'

@Controller('api/order')
export class OrdersController {

    constructor(
        private orderService: OrdersService,
    ){}


    @Post('/create')
    async Create(@Body() body: any) {
        return this.orderService.create(body);
    }

    @Get(':id')
    getOne(@Param('id') id: number){
        return this.orderService.searchOne(id);
    }

    
    @Get()
    getAll(){
        return this.orderService.searchAll();
    }

    @Put(':id')
    update(@Param('id') id: number, @Body() body: any) {
        return this.orderService.update(id, body);
    }

}
