# PruebaPeriferia_OmarCastellanos

## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/omarCastellanos/pruebaperiferia_omarcastellanos.git
git branch -M main
git push -uf origin main
```

# NOTAS OMAR CASTELLANOS

## descripcion

- Buenos dias, el proyecto se realizo en NestJS, con base de datos Postgres en Docker, y con typeORM para la comunicacion con la base de datos, corre bajo la version NodeJS 20.10.0

## contenedor docker con postgres

El contenedor Docker se corrio con los siguientes Comandos:

$ docker-compose up -d dabatase

se deben encontrar en el mismo nivel que el archivo docker-compose.yml

## migracion de entidades

se utilizan los comandos de la siguiente manera:

$ npm run migrations:generate 

$ npm run migrations:run 

igualmente se encuentran los querys en el archivo dentro de la carpeta src/database


# Iniciar la app

instalar dependencias
$ npm i

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod

## Pruebas por postman:

En el proyecto envio tambien el archivo Periferia-app.postman_collection.json con los llamados al API

Ejecutarlos de la siguiente forma:

1. crear restaurante
2. crear menu
3. crear Repartidor
4. crear pago
5. crear Pedido

Esto ya que existen relaciones entre las tablas

# FIN NOTAS OMAR



## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/omarCastellanos/pruebaperiferia_omarcastellanos/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

